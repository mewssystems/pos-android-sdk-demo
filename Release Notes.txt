Release notes 
-------------


SDK 1.1.7

 * Fix for bug which could cause crash if certain Bluetooth devices are already paired to the smartphone/tablet
 * Fix for bug in which successfull card not present reversing (via refund) of transaction would be marked as failed 
 * Internal modifications for future releases (installments, moto)