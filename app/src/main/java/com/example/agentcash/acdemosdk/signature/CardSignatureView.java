package com.example.agentcash.acdemosdk.signature;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.example.agentcash.acdemosdk.R;


/**
 * View that draws a signature bitmap over a card back side image asset.
 */
public class CardSignatureView extends View {

    Bitmap signatureBitmap;
    Bitmap cardBitmap;
    Rect destRect = new Rect();

    public CardSignatureView(Context context) {
        super(context);
    }

    public CardSignatureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(attrs);
    }

    public CardSignatureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(attrs);
    }

    private void initView(AttributeSet attrs) {
        cardBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.card_back_side);
    }

    public void setSignatureBitmap(Bitmap signatureBitmap) {
        this.signatureBitmap = Bitmap.createBitmap(signatureBitmap);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        destRect.set(0, 0, getWidth(), getHeight());
        canvas.drawBitmap(cardBitmap, null, destRect, null);

        if (signatureBitmap != null) {
            // Set signature bitmap width to occupy 60% of card bitmap width.
            // Let height to scale proportionally.
            int signatureTargetWidth = Math.round(getWidth() * .6f);
            float signatureScaleFactor = (float) signatureBitmap.getWidth()
                    / (float) signatureTargetWidth;
            int signatureTop = Math.round(getHeight() * 0.25f);
            int signatureLeft = Math.round(getWidth() * 0.05f);
            destRect.set(signatureLeft,
                    signatureTop,
                    signatureLeft + signatureTargetWidth,
                    signatureTop + Math.round(signatureBitmap.getHeight() / signatureScaleFactor));
            canvas.drawBitmap(signatureBitmap, null, destRect, null);
        }

    }

}
