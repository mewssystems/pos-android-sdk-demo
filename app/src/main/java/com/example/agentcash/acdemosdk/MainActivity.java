package com.example.agentcash.acdemosdk;

import android.content.Intent;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.agentcash.sdk.CardReaderParameters;
import com.agentcash.sdk.Payment;
import com.agentcash.sdk.PaymentsApi;
import com.agentcash.sdk.PaymentsSdk;
import com.agentcash.sdk.ProcessingMethod;
import com.agentcash.sdk.TerminalApi;
import com.agentcash.sdk.TerminalUpdateCheckListener;
import com.agentcash.sdk.TerminalUpdateCheckResult;
import com.agentcash.sdk.TerminalUpdateListener;
import com.agentcash.sdk.TerminalUpdateProgressStep;
import com.agentcash.sdk.TerminalUpdateResult;
import com.agentcash.sdk.TransactionError;
import com.agentcash.sdk.TransactionListener;
import com.agentcash.sdk.TransactionParameters;
import com.agentcash.sdk.TransactionProgressStep;
import com.example.agentcash.acdemosdk.signature.CardSignatureView;
import com.example.agentcash.acdemosdk.signature.SignatureActivity;

import java.io.IOException;
import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity implements TransactionListener {

    private final static String CURRENCY_CODE = "GBP";
    private static final String ACTION_USB_PERMISSION = "com.agentcash.register.USB_PERMISSION";
    
    private EditText usernameView;
    private EditText passwordView;
    private EditText chargeAmountView;
    private TextView messageLabelView;
    private CardSignatureView signatureImageView;
    private View signatureButtons;
    private Button refundButton;
    private Button fetchButton;

    private String verificationTranId;
    private String refundId;
    private BigDecimal refundAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usernameView = (EditText)findViewById(R.id.editUsername);
        passwordView = (EditText)findViewById(R.id.editPassword);
        chargeAmountView = (EditText)findViewById(R.id.chargeAmount);
        messageLabelView = (TextView)findViewById(R.id.messageView);
        signatureImageView = (CardSignatureView)findViewById(R.id.card_signature_image);
        signatureButtons = findViewById(R.id.sigButtons);
        refundButton = (Button) findViewById(R.id.refundButton);
        fetchButton = (Button) findViewById(R.id.fetchButton);
    }

    private PaymentsSdk getPaymentsSdk() {
        return DemoApp.getPaymentsSdk();
    }

    private PaymentsApi getPaymentsApi() {
        return getPaymentsSdk().getPaymentsApi();
    }

    private TerminalApi getTerminalApi() {
        return getPaymentsSdk().getTerminalApi();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPaymentsApi().setTransactionListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getPaymentsApi().setTransactionListener(null);
    }

    public void onChargeClick(View v) {
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        getPaymentsSdk().initialize(username, password, new PaymentsSdk.InitializationListener() {
            @Override
            public void onInitializationSucceeded() {
                Log.i("SDKDemo", "Registered payment API");
                startPayment();
            }

            @Override
            public void onInitializationFailed(PaymentsSdk.InitializationError error, String errorMessage) {
                Log.e("SDKDemo", "Error registering payment API: " + error + ": " + errorMessage);
                messageLabelView.setText("Error registering payment API: " + error + ": " + errorMessage);
            }
        });
    }

    public void onRefundClick(View v) {

        if (refundId != null) {
            signatureImageView.setVisibility(View.INVISIBLE);

            TransactionParameters transactionParams = TransactionParameters.Builder.refund(refundAmount, CURRENCY_CODE, refundId)
                    .setProcessingMethod(ProcessingMethod.CARD_NOT_PRESENT)
                    .build();

            // card reader can be null because we are allowing CNP refunds
            getPaymentsApi().startTransaction(transactionParams, null);
        }
    }

    public void onFetchClick(View v) {

        if (refundId != null) {
            signatureImageView.setVisibility(View.INVISIBLE);

            getPaymentsApi().fetchPayment(refundId, new PaymentsApi.PaymentFetchListener() {
                @Override
                public void onFetchSucceeded(@NonNull Payment payment) {
                    messageLabelView.setText("Payment successfully fetched: " + payment.getCardType());
                }

                @Override
                public void onFetchFailed(@NonNull PaymentsApi.FetchErrorType type, int httpStatusCode, @Nullable String message) {
                    messageLabelView.setText("Error fetching payment: " + type + "/" + httpStatusCode + ": " + message);
                }
            });
        }
    }

    private void startPayment() {

        signatureImageView.setVisibility(View.INVISIBLE);

        BigDecimal amount;
        try {
            amount = new BigDecimal(chargeAmountView.getText().toString());
        } catch (NumberFormatException e) {
            Log.e("SDKDemo", "Invalid amount");
            return;
        }

        String customExternalReference = "EXT. REF. #xxx";

        try {
            CardReaderParameters cardReader = CardReaderParameters.Builder.bluetooth()
                    .forModel(CardReaderParameters.Model.MIURA_MPI)
                    .build();

            // wifi example
//            CardReaderParameters cardReader = CardReaderParameters.Builder.tcp("192.168.1.90", 6543)
//                    .setName("wifi1")
//                    .build();

            // usb example
//            UsbManager usbManager = Objects.requireNonNull((UsbManager) getSystemService(Context.USB_SERVICE));
//            HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
//            UsbDevice usbDevice = (UsbDevice) usbDevices.values().toArray()[0];
//            if (!usbManager.hasPermission(usbDevice)) {
//                PendingIntent usbPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
//                usbManager.requestPermission(usbDevice, usbPermissionIntent);
//                return;
//            }
//            CardReaderParameters cardReader = CardReaderParameters.Builder.usb(usbDevice)
//                    .setName("Usb M020")
//                    .build();

            // *****
            // if a merchant is a payment facilitator, the sub-merchant must be registered on first payment.
            // this is done either setting full sub-merchant info (which will create or update sub-merchant)

            //SubMerchantInfo s = new SubMerchantInfo("SBM2", "name", "Street address 1", "City 1", "Postal 1");
            //getPaymentApi().setUpSubMerchant(s);

            // or if sub-merchant already is registered, then it is enough just to set its account id (for example if
            // you are setting up sub-merchants from different system)

//            getPaymentApi().setUpSubMerchant("SBM1");

            // it is enough to set sub-merchant once for paymentAPI, and then just use .onSubMerchant() when building
            // transaction parameters to notify API to execute on that (lastly set) sub-merchant.
            // *****

            TransactionParameters transactionParams = TransactionParameters.Builder.charge(amount, CURRENCY_CODE)
                    .setExternalId(customExternalReference)
//                    .onSubMerchant() // the transaction is being executed for sub-merchant
                    .build();

            getPaymentsApi().startTransaction(transactionParams, cardReader);

        } catch (Exception e) {
            messageLabelView.setText("Exception: " + e.getMessage());
            Log.e("SDKDemo", "Exception: " +  e.getMessage());
            Log.e("SDKDemo", "Stack trace: " +  e.getStackTrace().toString());
        }
    }

    @Override
    public void onTransactionProgress(final String transactionId, final TransactionProgressStep progressStep) {
        Log.i("SDKDemo", "onTransactionProgress " + transactionId + " : " + progressStep);
        messageLabelView.setText(progressStep.toString());
    }


    @Override
    public void onTransactionSucceeded(final String transactionId, final boolean isPaymentRefreshed) {
        Log.i("SDKDemo", "onTransactionSucceeded: " + transactionId);
        signatureImageView.setVisibility(View.INVISIBLE);
        signatureButtons.setVisibility(View.GONE);

        Payment payment = getPaymentsApi().getPayment(transactionId);
        if (payment.isRefundable()) {
            messageLabelView.setText("Payment success for transaction of " + payment.getAmount()
                    + " externally identified as: " + payment.getExternalId());
            setRefund(payment.getId(), payment.getAmount());
        } else {
            messageLabelView.setText("Refund succeeded for transaction of " + payment.getAmount()
                    + " externally identified as: " + payment.getExternalId());
        }
    }

    @Override
    public void onTransactionFailed(final String transactionId, final TransactionError error) {
        Log.e("SDKDemo", "onTransactionFailed: " + error);
        signatureImageView.setVisibility(View.INVISIBLE);
        signatureButtons.setVisibility(View.GONE);

        messageLabelView.setText("Payment failed: " + error.getError().toString() + "\n"
                + (error.getMessage() == null ? "" : error.getMessage()));
        setRefund(null, null);
    }

    @Override
    public void onSignatureRequired(final String transactionId) {
        Log.i("SDKDemo", "onSignatureRequired ");

        // set the ignoreSignatureCollection so no signature collection will be done
        boolean ignoreSignatureCollection = false;
        if (ignoreSignatureCollection) {
            getPaymentsApi().signatureCollectionIgnored(transactionId);
        } else {
            Payment payment = getPaymentsApi().getPayment(transactionId);
            Intent intent = new Intent(this, SignatureActivity.class);
            intent.putExtra(SignatureActivity.EXTRA_PAYMENT_AMOUNT, payment.getAmount().toString()
                    + " " + payment.getCurrency());
            intent.putExtra(SignatureActivity.EXTRA_TRANSACTION_ID, transactionId);
            startActivity(intent);
        }
    }

    @Override
    public void onSignatureVerificationRequired(final String transactionId, final Bitmap signatureBitmap, final boolean isRequired) {
        Log.i("SDKDemo", "onSignatureVerificationRequired ");
        messageLabelView.setText("Signature verification required.");

        if (isRequired) {
            verificationTranId = transactionId;
            signatureImageView.setVisibility(View.VISIBLE);
            signatureImageView.setSignatureBitmap(signatureBitmap);
            signatureButtons.setVisibility(View.VISIBLE);
        } else {
            getPaymentsApi().signatureVerificationCompleted(transactionId, true);
        }
    }

    public void onSigNOK(View v) {
        if (verificationTranId != null) {
            getPaymentsApi().signatureVerificationCompleted(verificationTranId, false);
        }
    }

    public void onSigOK(View v) {
        if (verificationTranId != null) {
            getPaymentsApi().signatureVerificationCompleted(verificationTranId, true);
        }
    }

    private void setRefund(final String paymentId, final BigDecimal amount) {
        refundId = paymentId;
        refundAmount = amount;
        if (paymentId == null) {
            refundButton.setVisibility(View.GONE);
            fetchButton.setVisibility(View.GONE);
        } else {
            refundButton.setText("Refund " + amount + " (" + paymentId + ")");
            refundButton.setVisibility(View.VISIBLE);

            fetchButton.setText("reload (fetch) payment " + paymentId);
            fetchButton.setVisibility(View.VISIBLE);
        }
    }

    public void onUpdateTerminalClick(View v) {
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        // if paymentsSdk is initialized, there is no need to initialize it again
        getPaymentsSdk().initialize(username, password, new PaymentsSdk.InitializationListener() {
            @Override
            public void onInitializationSucceeded() {
                Log.i("SDKDemo", "Registered payment API");
                terminalUpdateCheck();
            }

            @Override
            public void onInitializationFailed(PaymentsSdk.InitializationError error, String errorMessage) {
                Log.e("SDKDemo", "Error registering payment API: " + error + ": " + errorMessage);
                messageLabelView.setText("Error registering payment API: " + error + ": " + errorMessage);
            }
        });
    }

    private void terminalUpdateCheck() {
        CardReaderParameters cardReader = CardReaderParameters.Builder.bluetooth()
                .forModel(CardReaderParameters.Model.MIURA_MPI)
                .build();

        messageLabelView.setText("Terminal check started. Please wait...");
        getTerminalApi().doTerminalUpdateCheck(cardReader, new TerminalUpdateCheckListener() {
            @Override
            public void onCheckFinished(@NonNull TerminalUpdateCheckResult result, @Nullable String message) {
                messageLabelView.setText("Terminal check for updates finished with " + result + ". Details: " + message);

                // either show an alert asking user whether to upgrade, or leave 500msec timeout for device to close
                // after terminal check is done, otherwise terminal update will fail with "can not connect" error
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                if (result == TerminalUpdateCheckResult.UPDATE_RECOMMENDED || result == TerminalUpdateCheckResult.UPDATE_REQUIRED) {
                    terminalUpdate(cardReader);
                }
            }
        });
    }

    private void terminalUpdate(CardReaderParameters cardReader) {
        messageLabelView.setText("Starting terminal update ");
        getTerminalApi().doTerminalUpdate(cardReader, new TerminalUpdateListener() {
            @Override
            public void onUpdateFinished(@NonNull TerminalUpdateResult result, @Nullable String message) {
                messageLabelView.setText("Terminal update finished with " + result + ". Details: " + message);
            }

            @Override
            public void onUpdateProgress(@NonNull TerminalUpdateProgressStep progressStep, long progressInStep, long totalInStep) {
                messageLabelView.setText("Terminal update in progress " + progressStep
                        + (totalInStep > 0 ? " (" + progressInStep + " / " + totalInStep + ")" : "") + ". Still processing...");
            }
        });
    }
}
