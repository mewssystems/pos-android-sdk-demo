package com.example.agentcash.acdemosdk.signature;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.example.agentcash.acdemosdk.DemoApp;
import com.example.agentcash.acdemosdk.R;


public class SignatureActivity extends AppCompatActivity implements SignatureInfoView.OnSignatureInfoView {

    public static final String EXTRA_PAYMENT_AMOUNT = "com.example.agentcash.acdemosdk.signature.extra.payment_amount";
    public static final String EXTRA_CUSTOMER_SIGNATURE = "com.example.agentcash.acdemosdk.signature.extra.signature";
    public static final String EXTRA_TRANSACTION_ID = "com.example.agentcash.acdemosdk.signature.extra.transaction_id";

    SignatureInfoView signatureInfoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signature);
        signatureInfoView = (SignatureInfoView)findViewById(R.id.signature_info_view);

        String paymentAmount = getIntent().getStringExtra(EXTRA_PAYMENT_AMOUNT);

        signatureInfoView.setListener(this);
        signatureInfoView.setAmount(paymentAmount);
    }

    @Override
    public void onCancelClick() {
        showCancelConfirmation();
    }

    @Override
    public void onAuthorizeClick() {
        Log.d("SDKDemo", "SignatureInfoView authorize clicked");
        if (signatureInfoView.isEmptySignature()) {
            notifyMissingSignature();
        } else {
            Bitmap bitmap = signatureInfoView.signatureView.getDrawingCache();
            DemoApp.getPaymentsSdk().getPaymentsApi().signatureCollectionSucceeded(getIntent().getStringExtra(EXTRA_TRANSACTION_ID), bitmap);
            finish();
        }
    }

    private void showCancelConfirmation() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.signature_cancel);
        builder.setMessage(R.string.signature_transaction_cancellation_confirmation);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.d("SDKDemo", "SignatureInfoView cancel clicked.");
                Intent intent = new Intent();
                intent.putExtra(EXTRA_TRANSACTION_ID, getIntent().getStringExtra(EXTRA_TRANSACTION_ID));
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void notifyMissingSignature() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.charge_signature_required);
        builder.setMessage(R.string.signature_instruction);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void showErrorMessage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error_title);
        builder.setMessage(R.string.signature_error_upload);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}
