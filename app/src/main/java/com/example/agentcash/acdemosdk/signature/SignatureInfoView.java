package com.example.agentcash.acdemosdk.signature;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.agentcash.acdemosdk.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class SignatureInfoView extends RelativeLayout implements SignatureView.SignatureViewDelegate {

    public static String TAG = "SignatureInfoView";

    public SignatureView signatureView;
    Button cancelButton;
    Button clearButton;
    Button authorizeButton;
    View signatureLine;
    TextView signatureAmountView;
    private OnSignatureInfoView listener;


    public SignatureInfoView(Context context) {
        super(context);
        init();
    }

    public SignatureInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SignatureInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SignatureInfoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_signature_info, this, true);

        signatureView = (SignatureView)findViewById(R.id.signature_view);
        cancelButton = (Button)findViewById(R.id.signature_cancel);
        clearButton = (Button)findViewById(R.id.signature_clear);
        authorizeButton = (Button)findViewById(R.id.signature_authorize);
        signatureLine = (View)findViewById(R.id.signature_line);
        signatureAmountView = (TextView)findViewById(R.id.signature_amount);

        clearButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clear();
                clearButton.animate().alpha(0.0f);
                signatureView.setVisibility(View.VISIBLE);
            }
        });

        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCancelClick();
            }
        });

        authorizeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAuthorizeClick();
            }
        });


        signatureView.setDrawingCacheEnabled(true);
        signatureView.setDelegate(this);
        clearButton.setAlpha(0.0f);
    }

    public void setListener(OnSignatureInfoView listener) {
        this.listener = listener;
    }

    public boolean isEmptySignature() {
        return signatureView.isEmpty();
    }

    @Override
    public void onSignatureDetected() {
        clearButton.animate().alpha(1.0f);
    }

    public byte[] collectSignature() {

        Bitmap bitmap = signatureView.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 100, stream);
        final byte[] byteArray = stream.toByteArray();
        try {
            stream.close();
        } catch (IOException e) {
            Log.e("SDKDemo", "SignatureInfoView error during stream.close()");
            e.printStackTrace();
            return null;
        }
        return byteArray;
    }

    public void setAmount(final String amount) {
        signatureAmountView.setText(amount);
    }


    public interface OnSignatureInfoView {
        void onCancelClick();
        void onAuthorizeClick();
    }
}
