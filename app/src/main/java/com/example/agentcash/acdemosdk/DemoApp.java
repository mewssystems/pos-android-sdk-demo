package com.example.agentcash.acdemosdk;

import android.app.Application;;

import com.agentcash.sdk.PaymentsSdk;

public class DemoApp extends Application {

    private static PaymentsSdk paymentsSdk;

    public static PaymentsSdk getPaymentsSdk() {
        return paymentsSdk;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        paymentsSdk = new PaymentsSdk(this);
    }
}
