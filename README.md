# Demo application for AgentCASH SDK

## Introduction

This repository contains simple demo application which demonstrates the usage of AgentCASH for payments from 3rd party application via AgentCASH SDK.

This project contains all source code necessary to initiate payment and receive response from the AgentCASH app once payment has been executed.

Access the complete documentation [Bizzon - SDK Reference.3.0.0.pdf](Bizzon%20-%20SDK%20Reference.3.0.0.pdf).

 Visit us at  [https://www.bizzon.com/](https://www.bizzon.com/) to learn more about what we do.

## Build and run the application

Clone the repository

    $ git clone https://your_name@bitbucket.org/agentcash/agentcash-android-sdk-demo.git
    
Open the project in Android Studio

Build and run the application. Enter the amount to charge and press charge button.

## License

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.